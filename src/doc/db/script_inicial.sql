
create table user
(
  id    bigint auto_increment primary key,
  client_id  varchar(255) null,
  first_name varchar(255) null,
  last_name  varchar(255) null,
  password   varchar(255) null,
  salary     bigint       null,
  username   varchar(255) null
);


insert into user (client_id,first_name,last_name,password,salary,username)
values ('12skas1','Diego','Navia','57b3d16f6dfce06b02a15767eb021a0f8e762ef0f47457d8cb4270de223701839aad54f60667',1000000000,'diego');
