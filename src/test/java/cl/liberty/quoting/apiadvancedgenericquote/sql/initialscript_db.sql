INSERT INTO `line` VALUES
(1,'6','Vehículo'),
(2,'20','Incendio');

INSERT INTO `product` VALUES
(1,'969','Derco',1),
(2,'989','Full Worksite Individual',1),
(3,'993','Full Worksite Colectivo',1),
(4,'250','Incendio 22% - Sismo 10%',2);

insert into category values
(1,'Datos Personales'),
(2,'Datos Materia Asegurada'),
(3,'Pago'),
(4,'Datos del Riesgo'),
(5,'Datos del Asegurado');

insert into broker values
(1,'010','07965','SOC. CORREDORES DE SEGS.LOTA Y ASOCIADOS LTDA.','SOC. CORREDORES DE SEGS.LOTA Y ASOCIADOS LTDA.',null,null,'Hendaya',347663,'C_LOTA');

--insert into productbrokerrel (pb_rel_id, broker_id, product_id, restendpoint_id) values ();
insert into productbrokerrel values (1,1,1,1),(2,1,4,2);


INSERT INTO `question` VALUES
(1,'RutCorredor','rutCorredor',1,1),
(2,'RutSponsor','rutSponsor',1,1),
(3,'RutContratante','rutContratante',1,1),
(4,'DigContratante','digContratante',1,1),
(5,'RutAsegurado','rutAsegurado',1,1),
(6,'TipoVehiculo','tipoVehiculo',2,1),
(7,'Comuna','comuna',1,1),
(8,'DiasVigencia','diasVigencia',3,1),
(9,'FechaNacimiento','fechaNacimiento',1,1),
(10,'MarcaVehiculo','marcaVehiculo',2,1),
(11,'ModeloVehiculo','modeloVehiculo',2,1),
(12,'FormaPago','formaPago',3,1),
(13,'AnoVehiculo','anoVehiculo',2,1),
(14,'UsoDestino','usoDestino',2,1),
(15,'NumeroMotor','numeroMotor',2,1),
(16,'CantidadCuotas','cantidadCuotas',3,1),
(17,'MedidaSeguridad','medidaSeguridad',4,2),
(18,'Region','region',4,2),
(19,'Ciudad','ciudad',4,2),
(20,'Comuna','comuna',4,2),
(21,'TipoVivienda','tipoVivienda',4,2),
(22,'TipoConstruccion','tipoConstruccion',4,2),
(23,'Antiguedad','antiguedad',4,2),
(24,'UsoVivienda','usoVivienda',4,2),
(25,'CasaVivienda','casaVivienda',4,2),
(26,'CasaAdobe','casaAdobe',4,2),
(27,'DistanciaAgua','distanciaAgua',4,2),
(28,'DistanciaCosta','distanciaCosta',4,2),
(29,'Desabilitada','desabilitada',4,2),
(30,'CasaPareada','casaPareada',4,2),
(31,'Direccion','direccion',4,2),
(32,'CasaDepto','casaDepto',4,2),
(33,'Zona','zona',4,2),
(34,'FechaInicio','fechaInicio',4,2),
(35,'FechaTermino','fechaTermino',4,2),
(36,'EdificioUF','edificioUF',4,2),
(37,'ContenidoUF','contenidoUF',4,2),
(38,'Correo','correo',5,2),
(39,'Nombre','nombre',5,2),
(40,'ApellidoPaterno','apellidoPaterno',5,2),
(41,'ApellidoMaterno','apellidoMaterno',5,2),
(42,'TelefonoFijo','telefonoFijo',5,2),
(43,'TelefonoMovil','telefonoMovil',5,2);
--insert into form (pb_rel_id, question_id, cod_form) values ();
--insert into form (pb_rel_id, question_id, cod_form, restendpoint_id) values ();
INSERT INTO `form` VALUES
(1,1,11,1),
(1,2,11,1),
(1,3,11,1),
(1,4,11,1),
(1,5,11,1),
(1,6,11,1),
(1,7,11,1),
(1,8,11,1),
(1,9,11,1),
(1,10,11,1),
(1,11,11,1),
(1,12,11,1),
(1,13,11,1),
(1,14,11,1),
(1,15,11,1),
(1,16,11,1),
(2,17,22,2),
(2,18,22,2),
(2,19,22,2),
(2,20,22,2),
(2,21,22,2),
(2,22,22,2),
(2,23,22,2),
(2,24,22,2),
(2,25,22,2),
(2,26,22,2),
(2,27,22,2),
(2,28,22,2),
(2,29,22,2),
(2,30,22,2),
(2,31,22,2),
(2,32,22,2),
(2,33,22,2),
(2,34,22,2),
(2,35,22,2),
(2,36,22,2),
(2,37,22,2),
(2,38,22,2),
(2,39,22,2),
(2,40,22,2),
(2,41,22,2),
(2,42,22,2),
(2,43,22,2);

INSERT INTO `parameter` VALUES
(1,'value',''),
(2,'required','true'),
(3,'validateRut','true'),

(4,'key','rutCorredor'),
(5,'key','rutSponsor'),
(6,'key','rutContratante'),
(7,'key','digContratante'),
(8,'key','rutAsegurado'),
(9,'key','tipoVehiculo'),
(10,'key','comuna'),
(11,'key','diasVigencia'),
(12,'key','fechaNacimiento'),
(13,'key','marcaVehiculo'),
(14,'key','modeloVehiculo'),
(15,'key','formaPago'),
(16,'key','cantidadCuotas'),
(17,'key','anoVehiculo'),
(18,'key','usoDestino'),
(19,'key','nroMotor'),
(82,'key','medidaSeguridad'),
(86,'key','region'),
(87,'key','ciudad'),
(88,'key','comuna'),
(89,'key','tipoVivienda'),
(90,'key','tipoConstruccion'),
(91,'key','antiguedad'),
(92,'key','usoVivienda'),
(100,'key','casaVivienda'),
(104,'key','casaAdobe'),
(105,'key','distanciaAgua'),
(106,'key','distanciaCosta'),
(107,'key','desabilitada'),
(108,'key','casaPareada'),
(121,'key','direccion'),
(124,'key','casaDepto'),
(125,'key','zona'),
(126,'key','fechaInicio'),
(127,'key','fechaTermino'),
(128,'key','edificioUF'),
(129,'key','contenidoUF'),
(145,'key','correo'),
(146,'key','nombre'),
(147,'key','apellidoPaterno'),
(148,'key','apellidoMaterno'),
(149,'key','telefonoFijo'),
(150,'key','telefonoMovil'),

(20,'label','Rut Corredor'),
(21,'label','Rut Sponsor'),
(22,'label','Rut Contratante'),
(23,'label','Digito Contratante'),
(24,'label','Rut Asegurado'),
(25,'label','Tipo Vehiculo'),
(26,'label','Comunas'),
(27,'label','Dias Vigencia'),
(28,'label','Fecha Nacimiento'),
(29,'label','Marca'),
(30,'label','Modelo'),
(31,'label','Modalidad de Pago'),
(32,'label','Cantidad Cuotas'),
(33,'label','Año del Vehiculo'),
(34,'label','Uso Vehiculo'),
(35,'label','Nº Motor'),
(83,'label','Medidas de Seguridad para Cobertura de Robo'),
(93,'label','Región'),
(94,'label','Ciudad'),
(95,'label','Comuna'),
(96,'label','Tipo de Vivienda'),
(97,'label','Tipo Construcción'),
(98,'label','Antiguedad'),
(99,'label','Uso de Vivienda'),
(101,'label','¿Es casa en condominio?'),
(109,'label','Construcción de Adobe:'),
(110,'label','Distancia al cuerpo de agua o a lecho de no menor a 100 metros:'),
(111,'label','Distancia al borde costero menor a 25 msnm (metros sobre el nivel del mar):'),
(112,'label','Pasa deshabilitada por mas de 6 meses:'),
(113,'label','Es casa pareada por ambos costados:'),
(122,'label','Dirección'),
(130,'label','Casa/Depto'),
(131,'label','Zona'),
(132,'label','Fecha Inicio'),
(133,'label','Fecha Termino'),
(134,'label','Edificio UF'),
(135,'label','Contenido UF'),
(151,'label','E-mail'),
(152,'label','Nombre'),
(153,'label','Apellido Paterno'),
(154,'label','Apellido Materno'),
(155,'label','Teléfono Fijo'),
(156,'label','Teléfono Móvil'),

(36,'order','1'),
(37,'order','2'),
(38,'order','3'),
(39,'order','4'),
(40,'order','5'),
(41,'order','6'),
(42,'order','7'),
(43,'order','8'),
(44,'order','9'),
(45,'order','10'),
(46,'order','11'),
(47,'order','12'),
(48,'order','13'),
(49,'order','14'),
(50,'order','15'),
(51,'order','16'),
(137,'order','17'),
(138,'order','18'),
(139,'order','19'),
(140,'order','20'),
(141,'order','21'),

(52,'controlType','rut'),
(53,'controlType','textbox'),
(54,'controlType','dropdown'),
(55,'controlType','datepicker'),
(56,'controlType','selectdependent'),
(84,'controlType','checklist'),
(102,'controlType','radio'),
(123,'controlType','map'),

(57,'options','[]'),
(58,'options','[{"key":"5","value":"JEEP"},{"key":"1","value":"AUTOMOVIL"},{"key":"2","value":"STATION WAGON"},{"key":"3","value":"CAMIONETA"},{"key":"4","value":"FURGON"},{"key":"6","value":"TRAILER"},{"key":"8","value":"AMBULANCIA"},{"key":"9","value":"ESCOLAR"}]'),
-- (59,'options','[{"key":"21","value":"FORD"},{"key":"78","value":"NISSAN"},{"key":"61","value":"SUZUKI"}]'),
(60,'options','[{"key":"1","value":"Pago contado"},{"key":"2","value":"Aviso cuota"},{"key":"3","value":"Cuponera"},{"key":"4","value":"Pac"},{"key":"5","value":"Pat"}]'),
(61,'options','[{"key":"2019","value":"2019"},{"key":"2018","value":"2018"},{"key":"2017","value":"2017"},{"key":"2016","value":"2016"},{"key":"2015","value":"2015"},{"key":"2014","value":"2014"},{"key":"2013","value":"2013"},{"key":"2012","value":"2012"}]'),
(62,'options','[{"key":"1","value":"Particular"},{"key":"2","value":"Comercial"}]'),
(114,'options','[{"key":"1","value":"RM"}]'),
(115,'options','[{"key":"1","value":"Santiago"}]'),
(116,'options','[{"key":"1","value":"Santiago"}]'),
(117,'options','[{"key":"1","value":"Casa"},{"key":"2","value":"Depto inferior al 3er piso"},{"key":"3","value":"Depto 3er piso o superior"}]'),
(118,'options','[{"key":"1","value":"Sólido"},{"key":"2","value":"Ligero"}]'),
(119,'options','[{"key":"1","value":"Menor de 20 años"},{"key":"2","value":"Entre 20 y 40 años"},{"key":"3","value":"Entre 40 y 60 años"}]'),
(120,'options','[{"key":"1","value":"Solo habitacional"},{"key":"2","value":"Vacacional"},{"key":"3","value":"Habitacional y Comercial"},{"key":"4","value":"Comercial"}]'),
(136,'options','[{"key":"1","value":"Urbano"},{"key":"2","value":"Rural"}]'),

(63,'isParent','true'),
(64,'isParent','false'),

(65,'isChild','true'),
(66,'isChild','false'),

(67,'keyChild',''),
(68,'keyChild','modelo'),

(69,'keyParent',''),
(70,'keyParent','marca'),

(71,'entityNameChild',''),
(72,'entityNameChild','modelo'),

(73,'entityName','comuna'),
(74,'entityName','marca'),
(75,'entityName','modelo'),

(76,'minLength','7'),
(77,'minLength','8'),

(78,'maxLength','1'),
(79,'maxLength','12'),
(80,'maxLength','9'),

(81, 'positiveNum', 'true'),

(85,'items','[{"key":"item1","value":false,"label":"Condominio o Edificios con Guardias y/o Conserjes"},{"key":"item2","value":false,"label":"Depto. ubicado en 3º piso o superior"},{"key":"item3","value":false,"label":"Alarma conectada a central de vigilancia"}]'),
(103,'items','[{"key":"item","value":"SI","label":"Si"},{"key":"item","value":"NO","label":"No"}]'),

(142,'display','["","1"]'),
(143,'display','["2","3"]'),

(144,'depend','tipoVivienda'),

(157,'min','500');

-- (parameter_id,question_id)
INSERT INTO `question_param_rel` VALUES
(1,1),(2,1),(3,1),(4,1),(20,1),(36,1),(52,1),(77,1),(80,1),
(1,2),(2,2),(3,2),(5,2),(21,2),(37,2),(52,2),(77,2),(80,2),
(1,3),(2,3),(6,3),(22,3),(38,3),(53,3),(76,3),(79,3),
(1,4),(2,4),(7,4),(23,4),(39,4),(53,4),(78,4),
(1,5),(2,5),(3,5),(8,5),(24,5),(40,5),(52,5),(77,5),(80,5),
(1,6),(2,6),(9,6),(25,6),(41,6),(54,6),(58,6),
(1,7),(2,7),(10,7),(26,7),(42,7),(54,7),(57,7),(73,7),
(1,8),(2,8),(11,8),(27,8),(43,8),(53,8),(81,8),
(1,9),(2,9),(12,9),(28,9),(44,9),(55,9),
(1,10),(2,10),(13,10),(29,10),(45,10),(56,10),(57,10),(63,10),(66,10),(68,10),(69,10),(72,10),(74,10),
(1,11),(2,11),(14,11),(30,11),(46,11),(56,11),(57,11),(64,11),(65,11),(67,11),(70,11),(71,11),(75,11),
(1,12),(2,12),(15,12),(31,12),(47,12),(54,12),(60,12),
(1,13),(2,13),(17,13),(33,13),(48,13),(54,13),(61,13),
(1,14),(2,14),(18,14),(34,14),(49,14),(54,14),(62,14),
(1,15),(2,15),(19,15),(35,15),(50,15),(53,15),
(1,16),(2,16),(16,16),(32,16),(51,16),(53,16),(81,16),

(2,17),(82,17),(83,17),(141,17),(84,17),(85,17),
(2,18),(86,18),(93,18),(37,18),(54,18),(114,18),
(2,19),(87,19),(94,19),(38,19),(54,19),(115,19),
(2,20),(88,20),(95,20),(39,20),(54,20),(116,20),
(2,21),(89,21),(96,21),(43,21),(54,21),(117,21),
(2,22),(90,22),(97,22),(41,22),(54,22),(118,22),
(2,23),(91,23),(98,23),(47,23),(54,23),(119,23),
(2,24),(92,24),(99,24),(46,24),(54,24),(120,24),
(2,25),(100,25),(101,25),(44,25),(102,25),(103,25),(142,25),(144,25),
(104,26),(109,26),(42,26),(102,26),(103,26),
(105,27),(110,27),(48,27),(102,27),(103,27),
(106,28),(111,28),(49,28),(102,28),(103,28),
(107,29),(112,29),(50,29),(102,29),(103,29),
(108,30),(113,30),(51,30),(102,30),(103,30),
(2,31),(121,31),(122,31),(36,31),(123,31),
(1,32),(2,32),(124,32),(130,32),(45,32),(53,32),(143,32),(144,32),
(2,33),(125,33),(131,33),(40,33),(54,33),(136,33),
(2,34),(126,34),(132,34),(137,34),(55,34),
(2,35),(127,35),(133,35),(138,35),(55,35),
(1,36),(2,36),(128,36),(134,36),(139,36),(53,36),(81,36),(157,36),
(1,37),(2,37),(129,37),(135,37),(140,37),(53,37),(81,37),

(1,38),(2,38),(145,38),(151,38),(36,38),(53,38),
(1,39),(2,39),(146,39),(152,39),(37,39),(53,39),
(1,40),(2,40),(147,40),(153,40),(38,40),(53,40),
(1,41),(2,41),(148,41),(154,41),(39,41),(53,41),
(1,42),(2,42),(149,42),(155,42),(40,42),(53,42),
(1,43),(2,43),(150,43),(156,43),(41,43),(53,43);

insert into restendpoint  values
(1,'cotizador vehiculo derco lota')
,(2,'cotizador hogar');

-- (paramtree_id, param_key, param_value, param_tree_paramtree_id, question_id, restendpoint_id)
insert into paramtree  values
(1,'rutCorredor',null,null,1,1)
,(2,'rutSponsor',null,null,2,1)
,(3,'rutContratante',null,null,3,1)
,(4,'digContratante',null,null,4,1)
,(5,'rutAsegurado',null,null,5,1)
,(6,'tipoVehiculo',null,null,6,1)
,(7,'marcaVehiculo',null,null,10,1)
,(8,'modeloVehiculo',null,null,11,1)
,(9,'anoVehiculo',null,null,13,1)
,(10,'fechaNacimiento',null,null,9,1)
,(11,'comuna',null,null,7,1)
,(12,'producto','969',null,null,1)
,(13,'nroMotor',null,null,15,1)
,(14,'vIN',null,null,null,1)
,(15,'usoDestino','1',null,null,1)
,(16,'appKey','u04T6FSfokxza3BLNIVM3E6O8qfZKv7KmFvOhppq32oIlq5xyaqwhQvU_NzI9VuXAwAK60_1HSkYj4LkoWKh9g==',null,null,1)
,(17,'diasVigencia',null,null,8,1)
,(18,'formaPago',null,null,12,1)
,(19,'cantidadCuotas',null,null,16,1)
,(20,'mapValues',null,null,null,1)
,(21,'COMPANIA_ID','0',20,null,1),
(22,'MedidaSeguridad',null,null,17,2),
(23,'Region',null,null,18,2),
(24,'Ciudad',null,null,19,2),
(25,'Comuna',null,null,20,2),
(26,'TipoVivienda',null,null,21,2),
(27,'TipoConstruccion',null,null,22,2),
(28,'Antiguedad',null,null,23,2),
(29,'UsoVivienda',null,null,24,2),
(30,'CasaVivienda',null,null,25,2),
(31,'CasaAdobe',null,null,26,2),
(32,'DistanciaAgua',null,null,27,2),
(33,'DistanciaCosta',null,null,28,2),
(34,'Desabilitada',null,null,29,2),
(35,'CasaPareada',null,null,30,2),
(36,'Direccion',null,null,31,2),
(37,'CasaDepto',null,null,32,2),
(38,'Zona',null,null,33,2),
(39,'FechaInicio',null,null,34,2),
(40,'FechaTermino',null,null,35,2),
(41,'EdificioUF',null,null,36,2),
(42,'ContenidoUF',null,null,37,2),
(43,'Correo',null,null,38,2),
(44,'Nombre',null,null,39,2),
(45,'ApellidoPaterno',null,null,40,2),
(46,'ApellidoMaterno',null,null,41,2),
(47,'TelefonoFijo',null,null,42,2),
(48,'TelefonoMovil',null,null,43,2);

-- (requestdescription_id, description, param_key, param_value, type, restendpoint_id)
insert into requestdescription values
(1,null,'Content-Type','application/json','header-param',1)
,(2,null,'method','post','method-send',1)
,(3,null,'url','http://localhost:8080/advanced-generic-quote/v1.0.0/quote/home','uri',1)
,(4,null,'cot','23','param-uri',1)
,(5,null,'Content-Type','application/json','header-param',2)
,(6,null,'method','post','method-send',2)
,(7,null,'url','http://localhost:8080/advanced-generic-quote/v1.0.0/quote/home','uri',2)
,(8,null,'cot','23','param-uri',2);
--insert into restendpoint (restendpoint_id, environment, label) values ();
--insert into requestdescription (requestdescription_id, description, param_key, param_value, type, restendpoint_id) values ();
--insert into paramtree (paramtree_id, param_key, param_value, param_tree_paramtree_id, question_id, restendpoint_id) values ();
-- select * from question_param_rel;
-- select * from question;
-- select * from parameter;
-- insert into line (line_id, code, label) values ();
-- insert into product (product_id, code, label, line_id) values ();
-- insert into broker (broker_id, cod_sucursal, code, full_name, last_name, mail, name, name_sucursal, sperson, username) values ();
-- insert into productbrokerrel (pb_rel_id, broker_id, product_id, restendpoint_id) values ();
-- insert into question (question_id, label, category_id, pb_rel_id) values ();
-- insert into question_param_rel (parameter_id, question_id) values ();
-- insert into parameter (parameter_id, key_param, value_param) values ();
-- insert into restendpoint (restendpoint_id, label) values ();
-- insert into requestdescription (requestdescription_id, description, param_key, param_value, type, restendpoint_id) values ();
-- insert into paramtree (paramtree_id, param_key, param_value, param_tree_paramtree_id, question_id, restendpoint_id) values ();