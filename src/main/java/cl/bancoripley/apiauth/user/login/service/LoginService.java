package cl.bancoripley.apiauth.user.login.service;

import cl.bancoripley.apiauth.user.entity.UserAuthIn;
import cl.bancoripley.apiauth.user.entity.UserAuthOu;
import cl.bancoripley.apiauth.user.service.exception.ServiceValidatorException;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
public interface LoginService {

    /**
     * Check user with username and password
     * @param userAuthIn
     * @return
     */
    public UserAuthOu checkUsernameAndPass(UserAuthIn userAuthIn)
        throws ServiceValidatorException;

}
