package cl.bancoripley.apiauth.user.login.api;

import cl.bancoripley.apiauth.user.entity.User;
import cl.bancoripley.apiauth.user.entity.UserAuthIn;
import cl.bancoripley.apiauth.user.entity.UserAuthOu;
import cl.bancoripley.apiauth.user.login.service.LoginService;
import cl.bancoripley.apiauth.user.repository.UserRepository;
import cl.bancoripley.apiauth.user.service.exception.ServiceValidatorException;
import cl.bancoripley.apiauth.user.service.model.JsonResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
@RestController
@RequestMapping("user")
public class LoginController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    LoginService loginService;

    @PostMapping("/login")
    public ResponseEntity<JsonResponse> getInfoUser(@RequestBody UserAuthIn userIn){
        try {
            UserAuthOu uo = loginService.checkUsernameAndPass(userIn);
            return ResponseEntity.ok(new JsonResponse(HttpStatus.OK,uo));
        }catch (ServiceValidatorException e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.ok(
                    new JsonResponse(HttpStatus.UNAUTHORIZED,e.getMessage())
            );
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.ok(
                new JsonResponse(HttpStatus.INTERNAL_SERVER_ERROR,null)
            );
        }

    }


}
