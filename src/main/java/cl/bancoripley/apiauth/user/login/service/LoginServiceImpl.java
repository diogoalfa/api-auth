package cl.bancoripley.apiauth.user.login.service;

import cl.bancoripley.apiauth.user.entity.User;
import cl.bancoripley.apiauth.user.entity.UserAuthIn;
import cl.bancoripley.apiauth.user.entity.UserAuthOu;
import cl.bancoripley.apiauth.user.login.api.LoginController;
import cl.bancoripley.apiauth.user.repository.UserRepository;
import cl.bancoripley.apiauth.user.service.exception.ServiceValidatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.stereotype.Service;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
@Service
public class LoginServiceImpl implements LoginService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Value("${security.encrypt.secret}")
    String secret;
    @Value("${security.encrypt.key}")
    String key;

    public UserAuthOu checkUsernameAndPass(UserAuthIn userAuthIn)
            throws ServiceValidatorException {
        LOGGER.info("check login username:{}",userAuthIn.getUsername());
        User user = userRepository.findByUsername(userAuthIn.getUsername());
        if (userAuthIn.getPassword()
                .equals(decryptKey(user.getPassword()))) {
            LOGGER.debug("encontro al usuario");
            return convertUserOut(user);
        } else {
            throw new ServiceValidatorException("Usuario y Password incorrectos");
        }
    }

    private UserAuthOu convertUserOut(User user){
        UserAuthOu uo = new UserAuthOu();
        uo.setActive(true);
        uo.setAutorized(true);
        uo.setUsername(user.getUsername());
        return uo;
    }

    private String decryptKey(String text) {
        TextEncryptor textEncryptor = Encryptors.delux(secret,key);
        return textEncryptor.decrypt(text);
    }

    private String encryptKey(String text) {
        TextEncryptor textEncryptor = Encryptors.delux(secret,key);
        return textEncryptor.encrypt(text);
    }
}
