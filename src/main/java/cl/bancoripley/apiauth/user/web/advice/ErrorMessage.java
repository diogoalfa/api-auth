package cl.bancoripley.apiauth.user.web.advice;

import java.io.Serializable;
import java.util.Date;

import org.springframework.http.HttpStatus;

/**
 * @author Diego Navia
 *
 */
public class ErrorMessage implements Serializable {

	private static final long serialVersionUID = -5439088503262228772L;

	private final Date timeStamp;

	private final String message;

	private final int status;

	/**
	 * @param timeStamp
	 * @param message
	 * @param status
	 */
	public ErrorMessage(Date timeStamp, String message, int status) {
		this.timeStamp = timeStamp;
		this.message = message;
		this.status = status;
	}

	/**
	 * @param timeStamp
	 * @param message
	 * @param status
	 */
	public ErrorMessage(Date timeStamp, String message, HttpStatus status) {
		this(timeStamp, message, status.value());
	}

	/**
	 * @return the timeStamp
	 */
	public final Date getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @return the message
	 */
	public final String getMessage() {
		return message;
	}

	/**
	 * @return the status
	 */
	public final int getStatus() {
		return status;
	}

}
