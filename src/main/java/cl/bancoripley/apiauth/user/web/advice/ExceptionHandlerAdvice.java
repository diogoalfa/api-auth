package cl.bancoripley.apiauth.user.web.advice;

import java.util.Date;

import cl.bancoripley.apiauth.user.service.exception.ServiceValidatorException;
import cl.bancoripley.apiauth.user.service.model.JsonResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * @author Diego Navia
 */
@RestControllerAdvice
public class ExceptionHandlerAdvice {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerAdvice.class);

	@ExceptionHandler(ServiceValidatorException.class)
	public ResponseEntity<JsonResponse> serviceExceptions(ServiceValidatorException e) {

		LOGGER.error("Message Error[{}]", e.getMessage());
        return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED)
				.body(new JsonResponse(
						HttpStatus.PRECONDITION_FAILED,
						new ErrorMessage(new Date(), e.getMessage(), HttpStatus.PRECONDITION_FAILED)
						)
				);
	}

}
