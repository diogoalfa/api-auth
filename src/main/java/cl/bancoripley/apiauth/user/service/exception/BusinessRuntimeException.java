package cl.bancoripley.apiauth.user.service.exception;

/**
 * @author Diego Navia diego.navia@icloud.com
 *
 */
public class BusinessRuntimeException extends RuntimeException {

	private static final long serialVersionUID = -4757421325358562036L;

	/**
	 * @param message
	 */
	public BusinessRuntimeException(String message) {
		super(message);
	}

}
