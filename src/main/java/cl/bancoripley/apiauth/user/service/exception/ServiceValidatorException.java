package cl.bancoripley.apiauth.user.service.exception;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
public class ServiceValidatorException extends RuntimeException {

    private static final long serialVersionUID = -2011940209464259677L;

    /**
     * @param message
     */
    public ServiceValidatorException(String message) {
        super(message);
    }

}
