package cl.bancoripley.apiauth.user.entity;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
public class UserAuthOu {
    private String username;
    private boolean isAutorized;
    private boolean isActive;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isAutorized() {
        return isAutorized;
    }

    public void setAutorized(boolean autorized) {
        isAutorized = autorized;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "UserAuthOu{" +
                "username='" + username + '\'' +
                ", isAutorized=" + isAutorized +
                ", isActive=" + isActive +
                '}';
    }
}
