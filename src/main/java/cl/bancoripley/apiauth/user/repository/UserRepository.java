package cl.bancoripley.apiauth.user.repository;

import cl.bancoripley.apiauth.user.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    @Query("select u from User u where u.username = ?1")
    User findByUsername(String username);

    @Query("select u from User u where u.clientId = ?1")
    User findByUserClientId(String clientId);

}
