

## Tecnologías 

* sprint boot v2.1.7.RELEASE
* java 1.8
* mysql 8.0.17

# Lanzamiento

1. Crear Base datos : 

Nombre:test1

Host:mysql://localhost:3306/test1

2. ejecutar script :

link > https://gitlab.com/diogoalfa/api-auth/-/raw/master/src/doc/db/script_inicial.sql

3. ejecutar en consola : mvn spring-boot:run


### Endpoint

1. login:

* http://localhost:8080/api-auth/user/login
* method: POST
* BodyRequest:
```
{
	"username":"diego",
	"password":"clave1"
}
```

### Testing

* datos de prueba:

```
{
	"username":"diego",
	"password":"clave1"
}
```
* Respuesta exitosa :

```
{
    "status": "OK",
    "codeStatus": 200,
    "data": {
        "username": "diego",
        "active": true,
        "autorized": true
    }
}
```

* Respuesta erronea :

```
{
    "status": "Unauthorized",
    "codeStatus": 401,
    "data": "Usuario y Password incorrectos"
}
```